angular.module('starter.controllers', [])

    .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function() {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function() {
                $scope.closeLogin();
            }, 1000);
        };
    })


    .controller('AuthCtrl', function(Users, $scope, $location, $stateParams, $ionicHistory, $http, $state, $auth, $rootScope, $ionicLoading, $ionicPopup) {


        $scope.loginData = {}
        $scope.loginError = false;
        $scope.loginErrorText;

        $scope.login = function() {

            $ionicLoading.show();
            var credentials = {
                email: $scope.loginData.email,
                password: $scope.loginData.password
            }

            console.log(credentials);

            $auth.login(credentials).then(function() {

                $http({
                        method: 'POST',
                        url: DOMAIN + 'api/authenticate/user',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {
                            email: credentials['email'],
                            password: credentials['password']
                        }
                    }).success(function(response) {



                        // Stringify the retured data
                        var user = JSON.stringify(response.user);

                        // Set the stringified user data into local storage
                        localStorage.setItem('user', user);

                        // Getting current user data from local storage
                        $rootScope.currentUser = response.user;
                        // $rootScope.currentUser = localStorage.setItem('user');;

                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });

                        $state.go('app.appointments');
                    })
                    .error(function() {
                        $ionicLoading.hide();
                        $scope.loginError = true;
                        $scope.loginErrorText = error.data.error;
                        //console.log($scope.loginErrorText);
                    })
            }).catch(function(response) {
                $ionicLoading.hide();
                var alertPopup = $ionicPopup.alert({
                    title: 'Error en iniciar sessió',
                    template: "Dades d'inici de sessió incorrectes"
                });
            });
        }

    })

    .controller('CreateAppointmentCtrl', function(Services, $scope, $ionicLoading, $http, $ionicPopup, $state, $rootScope, $filter,$ionicHistory) {
        var _this = this;
        $scope.appointment = {};
        $scope.$on('$ionicView.enter', function() {
                $ionicLoading.show();
				$ionicHistory.nextViewOptions({
					disableBack: false
				});				
                Services.getServices().then(function(response) {
                    _this.services = response.data;
                }).catch(function(response) {

                }).finally(function() {
                    $ionicLoading.hide();
                });

            },

            $scope.submitForm = function() {
                $ionicLoading.show();
                $http({
                        method: 'POST',
                        url: DOMAIN + 'api/appointments/store',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {
                            start: $filter('date')($scope.appointment.start, 'dd/MM/yyyy HH:mm'),
                            end: $filter('date')($scope.appointment.end, 'dd/MM/yyyy HH:mm'),
                            service_id: $scope.appointment.service_id,
                            comment_professional: $scope.appointment.comment_professional,
                            user_id_professional: $rootScope.currentUser.id
                        }
                    }).success(function(response) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Operació realitzada',
                            template: "Cita creada correctament"
                        });
                        alertPopup.then(function(res) {
                            $ionicLoading.show();
							$ionicHistory.nextViewOptions({
								disableBack: true
							});								
                            $state.go('app.appointment', {
                                appointmentId: response
                            });

                        });
                    })
                    .error(function(response) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Operació NO realitzada',
                            template: response
                        });
                        //console.log(error.data.error);
                    })
            },
            $scope.ValidateDates = function() {
                if ($filter('date')($scope.appointment.end, 'dd/MM/yyyy HH:mm') <= $filter('date')($scope.appointment.start, 'dd/MM/yyyy HH:mm')) {
                    return false;
                } else {
                    return true;
                }
            }

        );
    })


    .controller('AppointmentsCtrl', function(Appointments, $scope, $ionicLoading, $rootScope, $location, $http, $state, $ionicPopup,$ionicHistory) {
        var _this = this;
		 //_this.appointments = [];
        $scope.$on('$ionicView.enter', function() {
                $ionicLoading.show();
				$ionicHistory.nextViewOptions({
					disableBack: false
				});					
                Appointments.getAppointments(($rootScope.currentUser != null) ? $rootScope.currentUser.id : -1, ($rootScope.currentUser != null) ? $rootScope.currentUser.roleId : -1, $location.url().substring(1).indexOf('appointmentsSelection')>=0 ? 1 : -1).then(function(response) {
                    _this.appointments = response.data;
                }).catch(function(response) {

                }).finally(function() {
                    $ionicLoading.hide();
                });
            },
            $scope.deleteAppointment = function(appointmentId) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Eliminar',
                    template: 'Segur que voleu eliminar la cita?'
                });

                confirmPopup.then(function(res) {
                    if (res) {
                        $ionicLoading.show();
                        $http({
                                method: 'POST',
                                url: DOMAIN + 'api/appointments/delete',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                transformRequest: function(obj) {
                                    var str = [];
                                    for (var p in obj)
                                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                    return str.join("&");
                                },
                                data: {
                                    appointmentId: appointmentId,
                                    userId: $rootScope.currentUser.id
                                }
                            }).success(function(response) {
                                Appointments.getAppointments(($rootScope.currentUser != null) ? $rootScope.currentUser.id : -1, ($rootScope.currentUser != null) ? $rootScope.currentUser.roleId : -1, $location.url().substring(1).indexOf('appointmentsSelection')>=0 ? 1 : -1).then(function(response) {
                                    _this.appointments = response.data;
                                    $ionicLoading.hide();
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Operació realitzada',
                                        template: "Cita eliminada correctament"
                                    });
                                }).catch(function(response) {
				
                                }).finally(function() {
                                    $ionicLoading.hide();
                                });
                            })
                            .error(function(response) {
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Operació NO realitzada',
                                    template: response
                                });
                               // console.log(error.data.error);
                            })
                    } else {

                    }
                });
            },
            $scope.cancelAppointment = function(appointmentId) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Cancel·lar',
                    template: 'Segur que voleu cancel·lar la cita?'
                });

                confirmPopup.then(function(res) {
                    if (res) {
                        $ionicLoading.show();
                        $http({
                                method: 'POST',
                                url: DOMAIN + 'api/appointments/cancel',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                transformRequest: function(obj) {
                                    var str = [];
                                    for (var p in obj)
                                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                    return str.join("&");
                                },
                                data: {
                                    appointmentId: appointmentId,
                                    userId: $rootScope.currentUser.id
                                }
                            }).success(function(response) {
                                Appointments.getAppointments(($rootScope.currentUser != null) ? $rootScope.currentUser.id : -1, ($rootScope.currentUser != null) ? $rootScope.currentUser.roleId : -1, $location.url().substring(1).indexOf('appointmentsSelection')>=0 ? 1 : -1).then(function(response) {
                                    _this.appointments = response.data;
                                    $ionicLoading.hide();
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Operació realitzada',
                                        template: "Cita cancel·lada correctament"
                                    });
                                }).catch(function(response) {
			
                                }).finally(function() {
                                    $ionicLoading.hide();
                                });
                            })
							.error(function(response) {
								$ionicLoading.hide();
								var alertPopup = $ionicPopup.alert({
									title: 'Operació NO realitzada',
									template: response
								});
								//console.log(error.data.error);
							})
                    } else {

                    }
                });
            },
            $scope.selectAppointment = function(appointmentId) {
                $ionicLoading.show();
                $http({
                        method: 'POST',
                        url: DOMAIN + 'api/appointments/select',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        transformRequest: function(obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {
                            appointmentId: appointmentId,
                            userId: $rootScope.currentUser.id
                        }
                    }).success(function(response) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Operació realitzada',
                            template: "Cita seleccionada correctament"
                        });
                        alertPopup.then(function(res) {
                            if (res) {
                                $ionicLoading.show();
                                $state.go('app.appointments');
                            } else {
                                $ionicLoading.show();
                                $state.go('app.appointments');
                            }
                        });

                    })
					.error(function(response) {
						$ionicLoading.hide();
						var alertPopup = $ionicPopup.alert({
							title: 'Operació NO realitzada',
							template: response
						});
						//console.log(error.data.error);
					})
            });
    })
    .controller('AppointmentCtrl', function(Appointments, $scope, $ionicLoading, $stateParams,$ionicHistory) {
        var _this = this;
        $scope.$on('$ionicView.enter', function() {
            $ionicLoading.show();
			$ionicHistory.nextViewOptions({
				disableBack: false
			});				
            Appointments.getAppointment($stateParams.appointmentId).then(function(response) {
                _this.appointments = response.data;
            }).catch(function(response) {

            }).finally(function() {
                $ionicLoading.hide();
            });
        });
    })




    .controller('AppointmentEditCtrl', function(Appointments, Services, $scope, $ionicLoading, $stateParams,$filter,$http,$state,$rootScope,$ionicPopup,$ionicHistory) {
        var _this = this;
        $scope.services = {};
		$scope.appointment = {};
        $scope.$on('$ionicView.enter', function()  {
            $ionicLoading.show();	
			$ionicHistory.nextViewOptions({
				disableBack: false
			});
            Appointments.getAppointment($stateParams.appointmentId).then(function(response) {
				Services.getServices().then(function(response) {
					$scope.services = response.data;
				});					
                $scope.appointment = response.data[0];
            }).catch(function(response) {

            }).finally(function() {
                $ionicLoading.hide();
            });								
        },
		$scope.submitForm = function() {
			$ionicLoading.show();
			$http({
					method: 'POST',
					url: DOMAIN + 'api/appointments/store',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					transformRequest: function(obj) {
						var str = [];
						for (var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: {
						start: $filter('date')($scope.appointment.start, 'dd/MM/yyyy HH:mm'),
						end: $filter('date')($scope.appointment.end, 'dd/MM/yyyy HH:mm'),
						service_id: $scope.appointment.service_id,
						comment_professional: $scope.appointment.comment_professional,
						id: $scope.appointment.id
					}
				}).success(function(response) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
						title: 'Operació realitzada',
						template: "Cita modificada correctament"
					});
					alertPopup.then(function(res) {
						$ionicLoading.show();
						
						$ionicHistory.nextViewOptions({
                            disableBack: true
                        });						
						
						$state.go('app.appointment', {
							appointmentId: response
						});

					});
				})
				.error(function(response) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
						title: 'Operació NO realitzada',
						template: response
					});
					//console.log(error.data.error);
				})
		},		
		$scope.ValidateDates = function() {
			if ($filter('date')($scope.appointment.end, 'dd/MM/yyyy HH:mm') <= $filter('date')($scope.appointment.start, 'dd/MM/yyyy HH:mm')) {
				return false;
			} else {
				return true;
			}
		})
		
	})	
    .controller('AppointmentEditClientCtrl', function(Appointments, $scope, $ionicLoading, $stateParams,$filter,$http,$state,$rootScope,$ionicPopup,$ionicHistory) {
        var _this = this;
		$scope.appointment = {};
        $scope.$on('$ionicView.enter', function()  {
            $ionicLoading.show();	
			$ionicHistory.nextViewOptions({
				disableBack: false
			});
            Appointments.getAppointment($stateParams.appointmentId).then(function(response) {				
                $scope.appointment = response.data[0];
            }).catch(function(response) {

            }).finally(function() {
                $ionicLoading.hide();
            });								
        },
		$scope.submitForm = function() {
			$ionicLoading.show();
			$http({
					method: 'POST',
					url: DOMAIN + 'api/appointments/store',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					transformRequest: function(obj) {
						var str = [];
						for (var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					},
					data: {
						comment_client: $scope.appointment.comment_client,
						id: $scope.appointment.id
					}
				}).success(function(response) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
						title: 'Operació realitzada',
						template: "Cita modificada correctament"
					});
					alertPopup.then(function(res) {
						$ionicLoading.show();
						
						$ionicHistory.nextViewOptions({
                            disableBack: true
                        });						
						
						$state.go('app.appointment', {
							appointmentId: response
						});

					});
				})
				.error(function(response) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
						title: 'Operació NO realitzada',
						template: response
					});
					//console.log(error.data.error);
				})
		},		
		$scope.ValidateDates = function() {
			if ($filter('date')($scope.appointment.end, 'dd/MM/yyyy HH:mm') <= $filter('date')($scope.appointment.start, 'dd/MM/yyyy HH:mm')) {
				return false;
			} else {
				return true;
			}
		})		
    });