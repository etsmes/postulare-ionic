// Ionic Starter App
var DOMAIN = 'http://85.59.68.246/postulare/public/';

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
// 'starter.services' is found in services.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'satellizer', 'permission', 'ion-datetime-picker'])

    .run(function($ionicPlatform, $rootScope, $auth, $state, $ionicHistory, $location, PermPermissionStore, $ionicPickerI18n) {

        $ionicPickerI18n.weekdays = ["dg", "dl", "dt", "dc", "dj", "dv", "ds"];
        $ionicPickerI18n.months = ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"];
        $ionicPickerI18n.cancel = "Cancel·lar";
        $ionicPickerI18n.title = "Escollir data i hora";


       /* $rootScope.$on('$stateChangeSuccess', function(event, current, previous) {
            $rootScope.actualLocation = $location.path();
            //alert($rootScope.actualLocation);	        
            if ($state.current.views.menuContent.title) $rootScope.title = $state.current.views.menuContent.title;

        })



        $rootScope.$watch(function() {
            return $location.path()
        }, function(newLocation, oldLocation) {
            if ($rootScope.actualLocation === newLocation) {
                //alert($state.current.views.menuContent.title);
                if ($state.current.views.menuContent.title) $rootScope.title = $state.current.views.menuContent.title;
            }
        });*/

        $rootScope.logout = function() {

            $auth.logout().then(function() {


                $ionicHistory.nextViewOptions({
                    disableBack: true,
                    historyRoot: true
                });
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache();

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;

                $state.go('app.auth');
            });
        }

        if (isJson(localStorage.getItem('user'))) {
            $rootScope.currentUser = JSON.parse(localStorage.getItem('user'));
        } else {
            $rootScope.currentUser = null;
        }

        // Define anonymous role
        PermPermissionStore
            .definePermission('anonymous', function(stateParams) {
                // If the returned value is *truthy* then the user has the role, otherwise they don't
                // var User = JSON.parse(localStorage.getItem('user'));
                // console.log("anonymous ", $auth.isAuthenticated());
                if (!$auth.isAuthenticated()) {
                    return true; // Is anonymous
                }
                return false;
            });

        PermPermissionStore
            .definePermission('isloggedin', function(stateParams) {
                // If the returned value is *truthy* then the user has the role, otherwise they don't
                // console.log("isloggedin ", $auth.isAuthenticated());
                if ($auth.isAuthenticated()) {
                    return true; // Is loggedin
                }
                return false;
            });

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $authProvider) {

        $authProvider.loginUrl = DOMAIN + 'api/authenticate';

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })



            .state('app.auth', {
                url: '/auth',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'AuthCtrl'
                    }
                }
            })


            .state('app.appointment_create', {
                url: '/appointment_create',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointment_create.html',
                        controller: 'CreateAppointmentCtrl',
                        controllerAs: 'services'
                    }
                }
            })

            .state('app.appointment', {
                url: '/appointment/:appointmentId',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointment.html',
                        controller: 'AppointmentCtrl',
                        controllerAs: 'appointments'
                    }
                }
            })
			
			
            .state('app.appointment_edit', {
                url: '/appointment_edit/:appointmentId',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointment_edit.html',
                        controller: 'AppointmentEditCtrl',
                        controllerAs: 'appointments'
                    }
                }
            })	

            .state('app.appointment_edit_client', {
                url: '/appointment_edit_client/:appointmentId',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointment_edit_client.html',
                        controller: 'AppointmentEditClientCtrl',
                        controllerAs: 'appointments'
                    }
                }
            })				

            .state('app.appointments', {
                url: '/appointments',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointments.html',
                        controller: 'AppointmentsCtrl',
                        controllerAs: 'appointments',
                        title: 'Les meves cites'
                    }
                }
            })

            .state('app.appointmentsSelection', {
                url: '/appointmentsSelection',
                data: {
                    permissions: {
                        except: ['anonymous'],
                        redirectTo: 'app.auth'
                    }
                },
                views: {
                    'menuContent': {
                        templateUrl: 'templates/appointments_select.html',
                        controller: 'AppointmentsCtrl',
                        controllerAs: 'appointments',
                        title: 'Seleccionar cita'
                    }
                }
            })


        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/auth');
    });

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};