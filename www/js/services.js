angular
    .module('starter.services', [])
    .factory('Appointments', function($http) {
        var dataSource = DOMAIN + 'api/appointments';

        return {
            getAppointments: function(p_userId, p_userRole, p_selectAppointment) {
                return $http.jsonp(dataSource + '?callback=JSON_CALLBACK', {
                    params: {
                        userId: p_userId,
                        userRole: p_userRole,
                        selectAppointment: p_selectAppointment
                    }
                });
            },
            getAppointment: function(p_appointmentId) {
                return $http.jsonp(dataSource + '/get?callback=JSON_CALLBACK', {
                    params: {
                        appointmentId: p_appointmentId
                    }
                });
            }
        }
    })
    .factory('Users', function($http) {
        var dataSource = DOMAIN + 'api/authenticate/user?callback=JSON_CALLBACK';

        return {
            getUser: function(p_credentials) {
                return $http.jsonp(dataSource, {
                    params: {
                        email: p_credentials.email,
                        password: p_credentials.password
                    }
                });
            }
        }
    })
    .factory('Services', function($http) {
        var dataSource = DOMAIN + 'api/services?callback=JSON_CALLBACK';

        return {
            getServices: function() {
                return $http.jsonp(dataSource, {

                });
            }
        }
    });